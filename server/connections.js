const app = require('express')();
const http = require('http').createServer(app);
const publicIp = require('public-ip');

async function startSocketIO() {
    const ip = await publicIp.v4();
    console.log(`PLAYERS CAN JOIN HERE: http://${ip}:3500`);
    return require('socket.io')(http, {
        cors: {
            origin: `http://${ip}:3500`,
            methods: ["GET", "POST"]
        }
    });
}

class Connections {
    constructor(io, onEvent) {
        this.io = io;
        http.listen(3499, () => {
            console.log('listening on *:3499');
        });

        this.io.on('connection', (socket) => {
            onEvent(socket.id, { event: "connection" });

            socket.on('disconnect', () => {
                onEvent(socket.id, { event: "disconnect" });
            });

            socket.on('event', (payload) => {
                console.log(`Received: `, payload);
                onEvent(socket.id, payload);
            });
        });
    }

    Emit(event) {
        this.io.emit('event', event);
        console.log("Emitting: ", event);
    }
}

module.exports = Connections;
Connections.startSocketIO = startSocketIO;
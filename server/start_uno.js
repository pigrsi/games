const Connections = require('./connections');
const UNO = require('../games/UNO/UNO');

class Server {
    constructor(io) {
        this.connections = new Connections(io, this.OnClientEvent.bind(this));
        this.game = new UNO(this.Emit.bind(this));
    }

    OnClientEvent(id, payload) {
        switch (payload.event) {
            case 'connection':
                this.Log(`Connected: ${id}`);
                this.game.EmitSync();
                break;
            case 'disconnect':
                this.Log(`Disconnected: ${id}`);
                break;
            case 'REQUEST_SEAT':
                this.game.RequestSeat(payload.name, payload.seatNumber);
                break;
            case 'PLAY_CARD':
                this.game.CardPlayRequested(payload.name, payload.cardIndex, payload.wildcardColor);
                break;
            case 'DRAW_CARD':
                this.game.CardDrawRequested(payload.name);
                break;
            case 'SKIP':
                this.game.RequestSkip(payload.name);
                break;
            case 'VOTE_END':
                this.game.VoteEnd(payload.name);
                break;
            case 'SET_RULE':
                this.game.SetRule(payload.id, payload.value);
                break;
            case 'START_GAME':
                this.game.Start();
                break;
        }
    }

    Emit(event) {
        this.connections.Emit(event);
    }

    Log(message) {
        console.log(message);
    }
}

Connections.startSocketIO().then((io) => {
    new Server(io)
});

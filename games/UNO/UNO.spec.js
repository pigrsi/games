const UNO = require("./UNO");
const TurnManager = UNO.TurnManager;

describe("UNO", () => {
    let game;

    beforeEach(() => {
        game = new UNO();
    })

    it("on game start, each player should have 7 cards", () => {
        game.Join("1", "Leo");
        game.Join("2", "Keb");
        game.Join("3", "Tog");
        game.Start();

        expect(game.state.hands["1"].length).toEqual(7);
        expect(game.state.hands["2"].length).toEqual(7);
        expect(game.state.hands["3"].length).toEqual(7);
        expect(game.state.draw.length).toEqual(87);
    });

    describe("Turn Manager", () => {
        it("Check legal cards", () => {
            testLegalCards("RED_0", ["RED_1"], ["RED_1"]);
            testLegalCards("RED_0", ["BLUE_0"], ["BLUE_0"]);
            testLegalCards("YELLOW_DRAW_2", ["GREEN_DRAW_2"], ["GREEN_DRAW_2"]);
            testLegalCards("YELLOW_DRAW_2", ["WILD"], ["WILD"]);
            testLegalCards("GREEN_8", ["GREEN_6", "BLUE_2", "WILD", "GREEN_SKIP", "RED_3"], ["GREEN_6", "WILD", "GREEN_SKIP"]);
        });

        it("Check wild4_restriction rule", () => {
            const rules = { stackingMode: "all", wild4_restriction: true };
            testLegalCards("RED_0", ["WILD_DRAW_4"], ["WILD_DRAW_4"], { rules });
            testLegalCards("RED_0", ["WILD_DRAW_4", "RED_2"], ["WILD_DRAW_4", "RED_2"], { toEqual: false, rules });
            testLegalCards("RED_0", ["RED_2", "WILD_DRAW_4"], ["RED_2", "WILD_DRAW_4"], { toEqual: false, rules });
            testLegalCards("RED_0", ["GREEN_0", "WILD_DRAW_4"], ["GREEN_0", "WILD_DRAW_4"], { rules });
            testLegalCards("RED_0", ["GREEN_5", "WILD_DRAW_4"], ["WILD_DRAW_4"], { rules });
        });

        function testLegalCards(discardCard, hand, expected, { toEqual=true, rules }={}) {
            if (!rules) rules = {stackingMode: "all", wild4_restriction: true};
            const turnManager = new TurnManager({ discard: [], }, rules);
            turnManager.PlayCard(discardCard);
            if (toEqual)
                expect(turnManager.GetLegalCardsInHand(hand)).toEqual(expected);
            else
                expect(turnManager.GetLegalCardsInHand(hand)).not.toEqual(expected);
        }
    });

});
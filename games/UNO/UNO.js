const UNODeck = require('./UNODeck');
const Helper = require('../GameHelper');

const LOBBY = 'LOBBY';
const GAME = 'GAME';
const MAX_PLAYERS = 8;

const DEBUG = (process.argv[2] === 'DEBUG');

class UNO {
    constructor(emit) {
        this.InitState();
        this.rules = {
            can_combo_draw: { options: [true, false], name: "Can Combo Draw cards", value: true },
            can_combo_effects: { options: [true, false], name: "Can Combo Effects", value: true },
            can_combo_numbers: { options: [true, false], name: "Can Combo Numbers", value: true },
            can_combo_colors: { options: [false, true], name: "Can Combo Colours", value: false },
            stackingMode: { options: ["all", "samecard", "none"], name: "Stacking Mode", value: "all" },
            wild4_restriction: { options: [false, true], name: "Wild 4 Restriction", value: false },
            free_draw: { options: [true, false], name: "Free Draw", value: true },
            reverse_skip_2p: { options: [true, false], name: "Reverse = Skip for 2P", value: true }
        };
        this.emit = emit;
        this.CreateLobby();
        this.stage = LOBBY;
    }

    Start() {
        if (this.stage === LOBBY && this.GetNumPlayers() > 1) {
            this.stage = GAME;
            this.CreateGame();
        }
    }

    InitState() {
        const emptySeats = [];
        emptySeats.length = MAX_PLAYERS;
        emptySeats.fill("");
        this.state = {
            seats: emptySeats,
            draw: [],
            hands: {},
            discard: [],
            turn: null
        };
    }

    BackToLobby() {
        if (this.stage === GAME) {
            this.stage = LOBBY;
            this.InitState();
            this.CreateLobby();
        }
    }

    GetNumPlayers() {
        return this.state.seats.filter(name => name !== "").length;
    }

    CreateLobby() {
        this.lobby = new Lobby(this.state, this.emit, this.rules);
    }

    CreateGame() {
        this.game = new Game(this.state, this.emit, this.BackToLobby.bind(this), this.rules);
    }

    SetRule(id, value) {
        if (this.stage === LOBBY) {
            if (this.rules[id]) {
                this.rules[id].value = value;
                this.EmitSync();
            }
        }
    }

    RequestSeat(name, seat) {
        if (this.stage === LOBBY) {
            this.lobby.RequestSeat(name, seat);
        }
    }

    RequestSkip(name) {
        if (this.stage === GAME) this.game.SkipRequested(name);
    }

    EmitSync() {
        if (this.stage === LOBBY) this.lobby.EmitSync();
        else if (this.stage === GAME) this.game.EmitSync();
    }

    CardPlayRequested(name, cardIndex, wildcardColor) {
        if (this.stage === GAME) this.game.CardPlayRequested(name, cardIndex, wildcardColor);
    }

    CardDrawRequested(name) {
        if (this.stage === GAME) this.game.CardDrawRequested(name);
    }

    VoteEnd(name) {
        if (this.stage === GAME) this.game.VoteEnd(name);
    }
}

const LEFT = 'LEFT';
const RIGHT = 'RIGHT';

class Game {

    constructor(state, emit, backToLobby, rules) {
        this.state = state;
        this.emit = emit;
        this.backToLobby = backToLobby;
        this.rules = rules;

        this.state.direction = RIGHT;
        this.state.color = "";
        this.state.legalCards = [];
        this.state.playersFinished = 0;
        this.cardsDrawn = 0;
        this.endVotes = [];

        this.InitialSetup();
    }

    InitialSetup() {
        this.state.deck = this.FreshDeck();
        if (!DEBUG) Helper.shuffle(this.state.deck);

        this.turnSkips = 0;

        this.state.seats = this.state.seats.filter(name => name !== "");
        this.state.drawStackSize = 0;
        this.turnManager = new TurnManager(this.state);

        this.turnManager.SetRandomTurn();
        this.EmitSync();

        this.state.seats.forEach(name => {
            this.state.hands[name] = this.state.deck.splice(0, 7);
        });

        this.EmitMove({ type: "Draw", hands: this.state.hands });
        this.EmitSync();

        this.EmitMove(this.turnManager.GetTurnChangeMove());

        const firstCardIndex = this.state.deck.findIndex(card => card !== 'WILD' && card !== 'WILD_DRAW_4');
        const firstCard = this.state.deck.splice(firstCardIndex, 1)[0];
        this.EmitMove({ type: "Deck Discard", value: firstCard });

        this.PlayCard(firstCard);

        this.EmitSync();
        this.StartFirstTurn();
    }

    EmitSync() {
        this.emit({ event: "GAME_SYNC",
            seats: this.state.seats,
            deck: this.state.deck.length,
            hands: this.state.hands,
            discard: this.state.discard.length === 0 ? "" : this.state.discard[this.state.discard.length - 1],
            legalCards: this.state.legalCards,
            turnPlayerCanSkip: this.state.turnPlayerCanSkip,
            color: this.state.color,
            direction: this.state.direction,
            playersFinished: this.state.playersFinished,
            ...this.turnManager.GetTurnSyncInfo()
        });
    }

    VoteEnd(name) {
        const votesRequired = Math.floor((this.state.seats.length + 1) / 2);
        if (this.state.seats.includes(name) && !this.endVotes.includes(name)) {
            this.endVotes.push(name);
        }
        if (this.endVotes.length >= votesRequired) {
            this.emit({ event: "BACK_TO_LOBBY" });
            this.backToLobby();
        }
    }

    EmitMove(move) {
        this.emit({
            event: "GAME_MOVE",
            ...move
        });
    }

    FreshDeck() {
        return UNODeck.map(value => value);
    }

    CardPlayRequested(name, cardIndex, wildcardColor) {
        if (cardIndex >= this.state.hands[name].length) return;
        const card = this.state.hands[name][cardIndex];
        if (this.CardPlayIsLegal(name, card)) {
            this.EmitSync();
            this.state.hands[name].splice(cardIndex, 1);
            this.EmitMove({ type: "Play Card", name, cardIndex, value: card });
            this.CheckHandOutOfCards(name);
            this.PlayCard(card);
            if (wildcardColor) this.state.color = wildcardColor;
            this.CalculateLegalCards(true);
            const keepSamePlayer = this.state.legalCards.length > 0;
            if (keepSamePlayer) this.state.turnPlayerCanSkip = true;
            this.StartNextTurn(keepSamePlayer);
        }
    }

    CheckHandOutOfCards(name) {
        if (this.state.hands[name].length === 0) {
            let numEmptyHands = 0;
            for (const nem in this.state.hands) {
                if (this.state.hands[nem].length === 0) numEmptyHands++;
            }
            this.EmitMove({ type: "Emptied Hand", name, position: numEmptyHands });
            this.state.playersFinished++;
        }
    }

    SkipRequested(name) {
        if (name === this.turnManager.GetTurnName() && this.state.turnPlayerCanSkip) {
            this.EmitSync();
            this.StartNextTurn();
        }
    }

    CardDrawRequested(name) {
        if (this.CanDrawCard(name)) {
            this.EmitSync();
            const numCardsToDraw = this.state.drawStackSize > 0 ? this.state.drawStackSize : 1;
            const drawnCards = [];
            for (let i = 0; i < numCardsToDraw; ++i) {
                const newCard = this.state.deck.pop();
                if (this.state.deck.length === 0) {
                    this.ResetDeck();
                    this.EmitSync();
                }
                drawnCards.push(newCard);
                this.state.hands[name].push(newCard);
                this.cardsDrawn++;
            }
            this.state.drawStackSize = 0;
            this.EmitMove({ type: "Draw", hands: { [name]: drawnCards } });
            this.EmitSync();
            if (this.cardsDrawn === 1) {
                // Can only play drawn card
                this.CalculateLegalCards();
                this.state.legalCards = this.state.legalCards.includes(drawnCards[0]) ? drawnCards : []
                this.state.turnPlayerCanSkip = true;
                this.EmitMove({ type: 'Select Card', turnName: name, legalCards: this.state.legalCards });
                this.EmitMove({ type: 'Can Skip', name, canSkip: this.state.turnPlayerCanSkip });
            } else {
                // Draw penalty, skip turn
                this.StartNextTurn();
            }
        }
    }

    ResetDeck() {
        this.state.deck = this.state.discard.splice(0, this.state.discard.length - 1);
        // Leave only the last discard card
        Helper.shuffle(this.state.deck);
    }

    CanDrawCard(name) {
        if (name === this.turnManager.GetTurnName()) {
            if (this.state.turnPlayerCanSkip) return false;
            if (this.state.drawStackSize > 0) return true;
            if (this.cardsDrawn > 0) return false;
            if (this.rules.free_draw.value) return true;
            return !this.HasLegalCard(name);
        }
    }

    StartFirstTurn() {
        if (this.turnSkips > 0) {
            this.turnSkips = 0;
            this.EmitMove(this.turnManager.GetTurnChangeMove());
            this.turnManager.RotateToNextTurn(this.state.direction);
        }
        this.EmitSync();
        this.EmitMove(this.GetNextMove());
    }

    StartNextTurn(combo=false) {
        if (!this.GameHasEnded()) {
            if (!combo) {
                this.state.turnPlayerCanSkip = false;
                this.EmitMove({ type: 'Can Skip', name: this.turnManager.GetTurnName(), canSkip: this.state.turnPlayerCanSkip });
                this.cardsDrawn = 0;
                this.turnManager.RotateToNextTurn(this.state.direction);
                while (this.turnSkips > 0) {
                    this.turnSkips--;
                    this.EmitMove(this.turnManager.GetTurnChangeMove());
                    this.turnManager.RotateToNextTurn(this.state.direction);
                }
                this.EmitMove(this.turnManager.GetTurnChangeMove());
            }
            this.EmitSync();
            this.EmitMove(this.GetNextMove(combo));
        }
    }

    GameHasEnded() {
        for (const name in this.state.hands) {
            if (this.state.hands[name].length !== 0) return false;
        }
        return true;
    }

    GetNextMove(combo=false) {
        const turnName = this.turnManager.GetTurnName();
        if (!combo) {
            this.state.legalCards = [];
            this.CalculateLegalCards();
        }

        return { type: 'Select Card', turnName, legalCards: this.state.legalCards };
    }

    PlayCard(card) {
        this.state.discard.push(card);

        if (card === 'WILD_DRAW_4') this.state.drawStackSize += 4;
        else if (card.includes('DRAW_2')) this.state.drawStackSize += 2;
        else if (card.includes('SKIP')) this.turnSkips++;
        else if (card.includes('REVERSE')) {
            this.state.direction = (this.state.direction === LEFT ? RIGHT : LEFT);
            if (this.rules.reverse_skip_2p.value && this.NumPlayersRemaining() === 2)
                this.turnSkips++
        }

        if (!isWildCard(card)) {
            this.state.color = getColor(card);
        }
    }

    NumPlayersRemaining() {
        return this.state.seats.length - this.state.playersFinished;
    }

    CalculateLegalCards(combo=false) {
        this.state.legalCards = this.GetLegalCardsInHand(this.turnManager.GetTurnHand(), combo);
    }

    CardPlayIsLegal(name, value) {
        return this.turnManager.GetTurnName() === name && this.state.legalCards.includes(value);
    }

    HasLegalCard(name) {
        return this.state.hands[name].some(card => this.state.legalCards.includes(card));
    }

    GetLegalCardsInHand(hand=[], combo=false) {
        const topCard = this.state.discard[this.state.discard.length - 1];
        const discardSymbol = getSymbol(topCard);
        let legal = [];
        if (this.state.drawStackSize > 0 && !combo) {
            if (this.rules.stackingMode.value === 'all') {
                legal = hand.filter(card => card.includes('DRAW'));
            } else if (this.rules.stackingMode.value === 'samecard') {
                legal = hand.filter(card => {
                    if (topCard === 'WILD_DRAW_4') return card === 'WILD_DRAW_4';
                    else if (topCard.includes('DRAW_2')) return card.includes('DRAW_2');
                });
            }
        } else {
            const that = this;
            let playerHasCardMatchingColor = false;
            let canMatchDraw = true;
            let canMatchEffect = true;
            let canMatchColor = true;
            let canMatchNumber = true;
            let canMatchSequentialNumber = true;
            if (combo) {
                if (this.cardsDrawn > 0) {
                    canMatchDraw = false;
                    canMatchEffect = false;
                    canMatchColor = false;
                    canMatchNumber = false;
                    canMatchSequentialNumber = false;
                } else {
                    canMatchDraw = this.rules.can_combo_draw.value;
                    canMatchEffect = this.rules.can_combo_effects.value;
                    canMatchColor = this.rules.can_combo_colors.value;
                    canMatchNumber = this.rules.can_combo_numbers.value;
                    canMatchSequentialNumber = canMatchNumber;
                }
            }
            legal = hand.filter(card => {
                // Combo rules
                if (combo && canMatchDraw) {
                    if (this.state.drawStackSize > 0 && isDrawCard(card)) return true;
                }
                if (!isWildCard(card) && canMatchEffect && isEffectCard(topCard) && isEffectCard(card)) {
                    if (getSymbol(topCard) === getSymbol(card)) return true;
                }


                // Regular rules
                if (!combo && isWildCard(card)) {
                    return true;
                }
                if (canMatchColor && getColor(card) === that.state.color) {
                    playerHasCardMatchingColor = true;
                    return true;
                }
                if (canMatchSequentialNumber) {
                    if (isNumberCard(topCard) && isNumberCard(card) && getColor(topCard) === getColor(card)) {
                        if (Math.abs((Number(getSymbol(topCard)) - Number(getSymbol(card)))) === 1) return true;
                    }
                }
                if (canMatchNumber && isNumberCard(topCard) && isNumberCard(card) && getSymbol(card) === discardSymbol) return true;
                return false;
            });
            if (this.rules.wild4_restriction.value && legal.includes("WILD_DRAW_4") && playerHasCardMatchingColor) {
                legal = legal.filter(card => card !== "WILD_DRAW_4");
            }
        }

        return legal;
    }
}

class TurnManager {
    constructor(state) {
        this.state = state;
        this.turnIndex = 0;
    }

    SetRandomTurn() {
        this.turnIndex = Math.floor(Math.random() * this.state.seats.length);
        if (DEBUG) this.turnIndex = 0;
    }

    RotateToNextTurn(direction) {
        do {
            const numPlayers = this.state.seats.length;
            const incrementBy = (direction === LEFT) ? -1 : 1;
            this.turnIndex += incrementBy;
            if (this.turnIndex < 0) this.turnIndex = numPlayers - 1;
            if (this.turnIndex >= numPlayers) this.turnIndex = 0;
        } while (this.GetTurnHand().length === 0);
    }

    GetTurnSyncInfo() {
        return { turnName: this.GetTurnName() };
    }

    GetTurnChangeMove() {
        return { type: "Set Turn", turnName: this.GetTurnName() };
    }

    GetTurnName() {
        return this.state.seats[this.turnIndex];
    }

    GetTurnHand() {
        return this.state.hands[this.GetTurnName()];
    }
}

class Lobby {

    constructor(state, emit, rules) {
        this.state = state;
        this.emit = emit;
        this.rules = rules;
    }

    RequestSeat(name, seat) {
        if (seat >= MAX_PLAYERS) {
            const currentSeat = this.state.seats.findIndex(inSeat => inSeat === name);
            if (currentSeat !== -1) this.state.seats[currentSeat] = "";
        }
        else if (!this.state.seats[seat]) {
            const currentSeatIndex = this.state.seats.findIndex(currentName => currentName === name);
            if (currentSeatIndex !== -1) this.state.seats[currentSeatIndex] = "";
            this.state.seats[seat] = name;
        }

        this.EmitSync();
    }

    EmitSync() {
        this.emit({ event: "LOBBY_SYNC", seats: this.state.seats, rules: this.rules });
    }
}

function isWildCard(card) {
    return card === "WILD" || card === "WILD_DRAW_4";
}

function getColor(card) {
    return card.split('_')[0];
}

function getSymbol(card) {
    if (isWildCard(card)) return "WILD";
    return card.split('_')[1];
}

function isNumberCard(card) {
    return !isEffectCard(card);
}

function isDrawCard(card) {
    return card.includes("DRAW");
}

function isEffectCard(card) {
    return isWildCard(card) || card.includes("REVERSE") || card.includes("SKIP") || card.includes("DRAW");
}

const UNOModule = module.exports = UNO;
UNOModule.TurnManager = TurnManager;
const uncoloured = [
    "0",
    "1",
    "1",
    "2",
    "2",
    "3",
    "3",
    "4",
    "4",
    "5",
    "5",
    "6",
    "6",
    "7",
    "7",
    "8",
    "8",
    "9",
    "9",
    "DRAW_2",
    "DRAW_2",
    "REVERSE",
    "REVERSE",
    "SKIP",
    "SKIP",
];

const red = uncoloured.map(card => "RED_" + card);
const yellow = uncoloured.map(card => "YELLOW_" + card);
const blue = uncoloured.map(card => "BLUE_" + card);
const green = uncoloured.map(card => "GREEN_" + card);
const neutral = [
    "WILD", "WILD", "WILD", "WILD", "WILD_DRAW_4", "WILD_DRAW_4", "WILD_DRAW_4", "WILD_DRAW_4",
]

module.exports = red.concat(yellow).concat(blue).concat(green).concat(neutral);
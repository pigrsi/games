const UNO = require('./games/UNO/UNO');
const UNODeck = require('./games/UNO/UNODeck');

export {
    UNO,
    UNODeck
}